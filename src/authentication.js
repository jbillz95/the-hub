import { auth, firestore } from './firebaseUtils.js';

export const authState = {
  isAuthed: false,
  user: {}
}

/*
  Launch Google oauth or set the auth state
*/
export function signIn() {
  const newSignIn = (user) => {
    if (user) {
      authState.isAuthed = true;
      return updateUserData(user);
    } else {
      auth().signInWithPopup(new auth.GoogleAuthProvider());
    }
  }

  return new Promise((resolve, reject) => {
    auth().onAuthStateChanged(newSignIn);
    resolve();
  });
};

/*
  fetch whatever user is currently authed to firebase
*/
export function getCurrentUser() {
  return firestore().collection('users').doc(auth().currentUser.uid).get().then((result) => {
    return result.data();
  }).catch((error) => {
    console.log(error);
  });
}

export function logoutCurrentUser() {
  return auth().signOut().then(() => {
    console.log('USER successfully signed out');
    return;
  }, (error) => {
    console.log(error);
  });
}

/*
  Takes in an object representing the user.
  If the user exists in firestore, we just return their data.
  Else, we write the new user data to firestore and return the result
*/
const updateUserData = (user) => {
  return firestore().collection('users').doc(user.uid).get().then((result) => {
    if (!result.exists) {
      console.log('User doesn\'t exist... creating new entry');
      writeUserData(user).then((result) => {
        return result;
      });
    } else {
      console.log('User already exists...');
      return result.data();
    }
  }, (error) => {
    console.log(`Error getting user data: ${error}`);
  });
}

/**
  Writes the user data to firestore
**/
const writeUserData = (user) => {
  const dataToWrite = {
    name: user.displayName,
    email: user.email,
    uid: user.uid,
    photoURL: user.photoURL
  };

  return firestore().collection('users').doc(user.uid).set(dataToWrite).then((result) => {
    return result;
  });
}
