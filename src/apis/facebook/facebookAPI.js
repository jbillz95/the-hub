const URL = require('url');
const queryString = require('querystring');
const _ = require('lodash');

const { auth, firestore } = require('../../firebaseUtils.js');

const hostname = 'https://localhost:5000';

const callAPI = async (params) => {
  const { route, queryParams } = params;
  let url = `${hostname}/${route}`;
  if (queryParams) {
    url = `${url}?${queryString.stringify(queryParams)}`;
  }
  const response = await fetch(url);
  const responseBody = await response.json();

  if (response.status !== 200) throw Error(responseBody.message);

  return responseBody;
}

const callPostAPI = async (params) => {
  const { route, data } = params;

  let url = `${hostname}/${route}`;

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: { 'Content-Type': 'application/json' }
  });
  const responseBody = await response.json();

  if (response.status !== 200) throw Error(responseBody.message);

  return responseBody;
}

const uploadAccessToken = (accessToken) => {
  const route = 'facebook/accessToken'; //TODO: Extract 'facebook' part of the route

  //TODO: Currently, we just get back the access token again. Receive something more meaningful?
  return callPostAPI({ route, data: { accessToken } }).then((results) => {
    return results;
  }).catch((error) => {
    console.log(error);
  });
}

const getFacebookProfile = () => {
  const route = 'facebook/me';

  return callAPI({ route }).then((profile) => {
    return profile;
  }).catch((error) => {
    console.log(error);
  });
}

const getFacebookTextPosts = () => {
  const route = 'facebook/myTextPosts';

  return callAPI({ route }).then((textPosts) => {
    return textPosts;
  }).catch((error) => {
    console.log(error);
  });
}

const getFacebookPicturePosts = () => {
  const route = 'facebook/myPicturePosts';
  return callAPI({ route }).then((picturePosts) => {
    return picturePosts;
  }).catch((error) => {
    console.log(error);
  });
}

const getFacebookPostMetrics = (postId) => {
  const route = `facebook/post/metrics/${postId}`;

  return callAPI({ route }).then((postMetrics) => {
    return postMetrics;
  }).catch((error) => {
    console.log(error);
  });
}

const updateFacebookPostInFirebase = (params) => {
  const firebaseUid = auth().currentUser.uid;
  const { postId, fieldsToUpdate } = params;

  return firestore().collection('facebook').doc(firebaseUid).collection('posts')
    .doc(postId)
    .update(fieldsToUpdate)
    .then(() => {
      return;
    }).catch((error) => {
      console.log(`There was an error updating post: ${postId} in Firestore`);
    });
};

const uploadFacebookPostToFirebase = (postData) => {
  const firebaseUid = auth().currentUser.uid;

  return firestore().collection('facebook').doc(firebaseUid).collection('posts').doc(postData.id)
    .set(postData)
    .then(() => {
      console.log('FB Post successfully written to firestore');
      return postData;
    }).catch((error) => {
      console.log(error);
    });
}

const getAllFacebookPostsFromFirebase = () => {
  const firebaseUid = auth().currentUser.uid;

  return firestore().collection('facebook').doc(firebaseUid).collection('posts')
    .get()
    .then((results) => {
      return _.map(results.docs, (doc) => doc.data());
    }).catch((error) => {
      console.log(error);
    });
}

module.exports = {
  getAllFacebookPostsFromFirebase,
  getFacebookPicturePosts,
  getFacebookPostMetrics,
  getFacebookProfile,
  getFacebookTextPosts,
  updateFacebookPostInFirebase,
  uploadAccessToken,
  uploadFacebookPostToFirebase
};
