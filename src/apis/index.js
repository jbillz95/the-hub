import {
  autoLoginUser,
  deleteTweet,
  getAllTweetsFromFirebase,
  getLatestTweet,
  getMostRecentTweetFromFirebase,
  getUsersLatestTweets,
  onLoginFailure,
  onLoginSuccess,
  postTweet,
  postTweetImg,
  updateUsersTweetsInFirebase,
  uploadTweetToFirebase
} from './twitter/twitterAPI';

import {
  getAllFacebookPostsFromFirebase,
  getFacebookPicturePosts,
  getFacebookPostMetrics,
  getFacebookProfile,
  getFacebookTextPosts,
  updateFacebookPostInFirebase,
  uploadAccessToken,
  uploadFacebookPostToFirebase
} from './facebook/facebookAPI';

export {
  autoLoginUser,
  deleteTweet,
  getAllFacebookPostsFromFirebase,
  getAllTweetsFromFirebase,
  getFacebookPostMetrics,
  getFacebookProfile,
  getFacebookTextPosts,
  getFacebookPicturePosts,
  getLatestTweet,
  getMostRecentTweetFromFirebase,
  getUsersLatestTweets,
  onLoginFailure,
  onLoginSuccess,
  postTweet,
  postTweetImg,
  updateFacebookPostInFirebase,
  uploadAccessToken,
  uploadFacebookPostToFirebase,
  updateUsersTweetsInFirebase,
  uploadTweetToFirebase
};
