const URL = require('url');
const queryString = require('querystring');
const _ = require('lodash');
const { auth, firestore } = require('../../firebaseUtils.js');
const { getCurrentUser } = require('../../authentication.js');

const hostname = 'https://localhost:5000';

/*
  This method is essentially the root of our twitter api.
  All GET requests to the backend will run through this method
*/
const callAPI = async (params) => {
  const { route, queryParams } = params;
  let url = `${hostname}/${route}`;
  if (queryParams) {
    url = `${url}?${queryString.stringify(queryParams)}`;
  }
  const response = await fetch(url);
  const body = await response.json();

  if (response.status !== 200) throw Error(body.message);

  return body;
}

/*
  This method is essentially the root of our twitter api.
  All POST requests to the backend will run through this method
  TODO: I think eventually we should move these two low-level
  methods to a 'baseApi' file.
*/
const callPostAPI = async (params) => {
  const { route, tweetData } = params;

  let url = `${hostname}/${route}`;

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(tweetData),
    headers: { 'Content-Type': 'application/json' }
  });
  const body = await response.json();

  if (response.status !== 200) throw Error(body.message);

  return body;
}

/*
  Take a tweet object (from twitter -> backend server),
  extract the id, upload the data to firebase
*/
const uploadTweetToFirebase = (tweetData) => {
  const firebaseUid = auth().currentUser.uid;

  return firestore().collection('twitter').doc(firebaseUid).collection('tweets').doc(tweetData.id)
    .set(tweetData)
    .then(() => {
      console.log('Tweet successfully written to firestore');
      return tweetData;
    }).catch((error) => {
      console.log(error);
    })
}

//TODO: This method's mostly a proof of concept. Should probably delete later
const getLatestTweet = (queryParams) => {
  const route = 'latestTweet';

  return callAPI({ route }).then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  })
}

/*
  Fetch the latest tweets for the current user.
*/
const getUsersLatestTweets = (params) => {
  const route = 'myLatestTweets';

  //queryParams acts as the default options
  const queryParams = {
    count: 5
  };

  //This allows the user to override number of tweets to retrieve
  _.merge(queryParams, params);

  return callAPI({ route, queryParams }).then((rawTweets) => {
    return rawTweets;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  })
}

/*
  Grab all of the user's tweets since the last_tweet_id from twitter,
  then upload all those new tweets to Firestore
*/
const updateUsersTweetsInFirebase = (params) => {
  const route = 'myLatestTweets';
  const queryParams = {
    count: 100,
    user_id: params.user_id,
    since_id: params.last_tweet_id,
    exclude_replies: true,
    exclude_rts: true
  }

  return callAPI({ route, queryParams }).then((rawTweets) => {

    if (_.isEmpty(rawTweets)) {
      console.log('No new tweets to push to Firestore...');
      return;
    }

    return _.map(rawTweets, (tweet) => {
      //TODO: Honestly, establishing a real datamodel might be the move...
      return {
        id: tweet.id_str,
        createdAt: new Date(tweet.created_at),
        text: tweet.text,
        retweetCount: tweet.retweet_count,
        favoriteCount: tweet.favorite_count,
        media: _.get(tweet, 'entities.media') || false,
        userId: tweet.user.id_str,
        userScreenName: tweet.user.screen_name,
        userName: tweet.user.name,
        popularity: (tweet.retweet_count + tweet.favorite_count)
      };
    });
  }).then((firebaseTweets) => {
    //Map the new tweets to an array of promises intent on uploading them to firebase
    return _.map(firebaseTweets, (tweet) => uploadTweetToFirebase(tweet));
  }).then((tweetPromises) => {
    return Promise.all(tweetPromises);
  }).then((updatedTweets) => {
    return updatedTweets;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const getMostRecentTweetFromFirebase = () => {
  const firebaseUid = auth().currentUser.uid;

  return firestore().collection('twitter').doc(firebaseUid).collection('tweets')
    .orderBy('createdAt', 'desc')
    .limit(1)
    .get()
    .then((results) => {
      return _.first(_.map(results.docs, (doc) => doc.data()));
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const getAllTweetsFromFirebase = () => {
  const firebaseUid = auth().currentUser.uid;

  return firestore().collection('twitter').doc(firebaseUid).collection('tweets')
    .get()
    .then((results) => {
      return _.map(results.docs, (doc) => doc.data());
    }).catch((error) => {
      console.log(error);
    });
}

const postTweet = (tweetData) => {
  const route = 'tweet';

  return callPostAPI({ route, tweetData }).then((result) => {
    return result;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const deleteTweet = (tweetData) => {
  const route = 'tweet/delete';

  return callPostAPI({ route, tweetData }).then((result) => {
    return result;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const postTweetImg = (tweetData) => {
  const route = 'tweetImg';

  return callPostAPI({ route, tweetData }).then((result) => {
    return result;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const autoLoginUser = (response) => {
  return callAPI({ route: 'currentUser' }).then((currentUser) => {
    return currentUser;
  }).catch((error) => {
    console.log(error);
  })
}

const onLoginSuccess = (response) => {
  const token = response.headers.get('x-auth-token');
  return response.json().then((user) => {
    if (token) {
      console.log('User successfully logged in');
      return callAPI({ route: 'currentUser' }).then((currentUser) => {
        return currentUser;
      });
    } else {
      console.log('We encountered an error logging into twitter?');
    }
  });
}

const onLoginFailure = (error) => {
  alert(error);
}

module.exports = {
  autoLoginUser,
  deleteTweet,
  getAllTweetsFromFirebase,
  getLatestTweet,
  getMostRecentTweetFromFirebase,
  getUsersLatestTweets,
  onLoginFailure,
  onLoginSuccess,
  postTweet,
  postTweetImg,
  updateUsersTweetsInFirebase,
  uploadTweetToFirebase
}
