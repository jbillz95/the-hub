import firebase from 'firebase';
require('firebase/firestore');

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBbr9899kQUWkDO2EoE62aiVDP-Kf1h338",
  authDomain: "the-hub-23a5c.firebaseapp.com",
  databaseURL: "https://the-hub-23a5c.firebaseio.com",
  projectId: "the-hub-23a5c",
  storageBucket: "the-hub-23a5c.appspot.com",
  messagingSenderId: "716720447024"
};

const app = firebase.initializeApp(config);
const firestore = firebase.firestore;
const auth = firebase.auth;

export { auth, firestore };
