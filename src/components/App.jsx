import React, { Component } from 'react';
import ReactDOM from "react-dom";
import PropTypes from 'prop-types';
import { auth, firestore } from '../firebaseUtils.js';
import { LandingPageContainer } from 'landing-page';
import createRoutes from './Routes.js';

import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return createRoutes();
  }
}

export default App;
