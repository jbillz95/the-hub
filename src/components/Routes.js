import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  IndexRoute,
  Redirect,
  Switch
} from 'react-router-dom';
import { DataAnalysisPageContainer } from 'data-analysis-page';
import { LandingPageContainer } from 'landing-page';
import { MainPageContainer } from 'main-page';
import { authState } from '../authentication.js';

//This route locks out pages if the user isn't authed
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    authState.isAuthed === true
      ? <Component {...props} />
      : <Redirect to='/' />
  )} />
)

const createRoutes = () => (
  <Router>
    <div>
      <Switch>
        <Route exact path='/' component={ LandingPageContainer }/>
        {/*TODO: Override about component when we've built the about page*/}
        <Route path='/about' component={ () => <h1>Placeholder</h1> }/>
        <PrivateRoute path='/dataAnalysis' component={ DataAnalysisPageContainer }/>
        <PrivateRoute path='/main' component={ MainPageContainer }/>
        <PrivateRoute component={ MainPageContainer }/>
      </Switch>
    </div>

  </Router>
);

export default createRoutes;
