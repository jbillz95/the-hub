import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './post.css';


const CheckBox = (props) => {
	return (
		<div>
			<input
			 	type='checkbox'
			 	name={ props.name }
				onChange={ props.onChange }
				checked={ props.checked }
			/>
			{ props.name }
		</div>
	);
}

CheckBox.propTypes = {
	checked: PropTypes.bool,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
};

export default CheckBox;
