import React from 'react';
import PropTypes from 'prop-types';
import './dropmenu.css';
import DropMenuItem from './DropMenuItem.jsx';

const DropMenu = (props) => {
	const dropMenuOptions = _.map(props.dropMenuOptions, (item) =>
		<DropMenuItem key={ item.id } value={ item.value } text={ item.text }/>
	);

	return(
		<div className='dropMenu'>
			<select onChange={ props.onChange }>
				<option defaultValue selected disabled>{ props.dropMenuDefault }</option>
				{ dropMenuOptions }
			</select>
		</div>
	);
}

DropMenu.propTypes = {
	onChange: PropTypes.func.isRequired,
	dropMenuDefault: PropTypes.string.isRequired,
	dropMenuOptions: PropTypes.array.isRequired
}

export default DropMenu;