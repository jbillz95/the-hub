import React from 'react';
import PropTypes from 'prop-types';
import { Post } from 'main-page';

const FacebookPost = (props) => {
  const metrics = {
    'Create Date': props.createDate,
    'Comments': props.commentCount,
    'Likes': props.likeCount
  };

  return (
    <Post
      className={ props.className }
      metrics={ metrics }
      type={ props.type }
      text={ props.text }
      imgUrl={ props.imgUrl }
    />
  );
};

FacebookPost.propTypes = {
  className: PropTypes.string,
  commentCount: PropTypes.number.isRequired,
  createDate: PropTypes.string.isRequired,
  likeCount: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
}
export default FacebookPost;
