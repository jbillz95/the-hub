import React from 'react';
import PropTypes from 'prop-types';
import './popup.css';
import { Button } from 'main-page';
const Popup = (props) => {
  return(
    <div className='popup'>
        <div className='popup_inner'>
		  <h1 className='popup_title'>{ props.text }</h1>
          <div className='popup_elements'>
            { props.elements }
          </div>
      { props.closePopup &&
		  <div className='popup_close'>
  			<Button
  				onClick={ props.closePopup}
  				text="Close Popup"
  			/>
		  </div>
      }
        </div>
    </div>
  );
};

Popup.propTypes = {
  elements: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  closePopup: PropTypes.func
};

export default Popup;
