import React from 'react';
import PropTypes from 'prop-types';
import './post.css';
import { PlatformImage } from 'main-page';

const Post = (props) => {
  const metrics = _.map(props.metrics, (value, field) => {
    return <span>{ field }: { value }</span>
  });


  return (
    <div className='postDiv'>
      <table className={ props.type } border="1">
					<col width='100px'/>
          <tr>
	          <td rowSpan='2' >
							<PlatformImage platformName={ props.type } />
						</td>
            <td className='postContent'> 
              <div>
                {!props.text.includes('https://') 
                    && props.text } 
              </div>
              <div>
                {props.imgUrl && 
                  <img 
                    height='150' 
                    width='150' 
                    src={props.imgUrl}
                  />
                 } 
              </div>
            </td>
          </tr>
          <tr>
            <td className='metrics' height='10'>
              <div>
                { metrics }
                <span></span>
                <button onClick={ () => props.deleteTweet(props.id) }>Delete</button> 
              </div>
            </td>
          </tr>
      </table>
    </div>
  );
};

Post.propTypes = {
	className: PropTypes.string,
  id: PropTypes.string,
  imgUrl: PropTypes.string,
  metrics: PropTypes.object,
	type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default Post;
