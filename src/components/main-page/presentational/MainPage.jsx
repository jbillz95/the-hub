import './MainPage.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TwitterLogin from 'react-twitter-auth';
import FacebookAuth from 'react-facebook-auth';

import {
	CheckBox,
	Post,
	Button,
	Popup,
	DropMenu,
	TwitterPost,
	FacebookPost,
	InputBox,
	Header
} from 'main-page';

const MainPage = (props) => {

  const twitterPostButton = (
    <Button
		onClick={ () => props.twitterTest(props.postTextState()) }
		text="Submit Text Post"
	/>
  );

  const twitterPostImgButton = (
    <Button
		onClick={ () => props.twitterTestImg(props.uploadImageDataUrlState()) }
		text="Submit Image Post"
	/>
  );

  const twitterLoginButton = (
    <TwitterLogin
      loginUrl='https://localhost:5000/api/login'
      onSuccess={ props.twitterLoginSuccess }
      onFailure={ props.twitterLoginFailure }
      requestTokenUrl='https://localhost:5000/api/login/reverse'
    />
  );

  const facebookLoginButton = ({ onClick }) => (
    <Button
      onClick={ onClick }
      text={ 'Login with Facebook' }
    />
  );

  //TODO: Move somewhere else...
  //Permission scopes for the access accessToken
  const scope = 'email, user_birthday, user_location, user_posts, user_likes, user_photos';
  const facebookLoginComponent = (
    <FacebookAuth
      appId='589163421464307'
      callback={ props.facebookLogin }
      component={ facebookLoginButton }
      scope={ scope }
    />
  );

  const dataAnalysisNavButton = (
    <Button
      onClick={ () => props.navigateToDataPageOnClick() }
      text={ 'Navigate to Data Analysis Page' }
    />
	);

  // post sort drop-down list
  const sortBy = (
	<DropMenu
		onChange={ props.changeSortState }
		dropMenuDefault={ 'Sort Posts By...' }
		dropMenuOptions={
			[
				{ id: 1, value: 'recent', text: 'Most Recent' },
				{ id: 2, value: 'popular', text: 'Most Popular' },
				{ id: 3, value: 'oldest', text: 'Oldest' },
				{ id: 4, value: 'least', text: 'Least Popular' }
			]
		}
	/>
  );

  //temp Search Bar
  const searchBar = (
    <div className='searchInput'>
      <input
        type='text'
        placeholder='Search...'
		onInput={ props.changeSearchState }
      />
    </div>
  );

  const signOut = (
    <Button
			id={ 'myButton' }
      onClick={ props.redirectToLanding }
			text={ 'Sign Out' }
		/>
  );
  
  const dateFilter = (
	<div className='dateFilter'>
		<h4>Filter Posts From</h4>
		<input
			type='date'
			name='after'
			placeholder="Posts After Date"
			onInput={ props.changeFilterState }>
		</input>
		<h4> to </h4>
		<input
			type='date'
			name='before'
			placeholder="Posts Before Date"
			onInput={ props.changeFilterState }>
		</input>
	</div>
  );

 const posts = (
    <div>
    {
      _.map(props.getPostsToRender(), (post) => {
        const date = post.postData.createdAt;
        const dateStr = date.toString();
        const formatDate = dateStr.substring(0, dateStr.length - 23);
        let imgUrl;

        if (post.postData.media){
					imgUrl = _.get(post, 'postData.media[0].media_url_https');
        }

        if (post.name === 'twitter') {
          return (
            <TwitterPost
              createDate={ formatDate }
              deleteTweet={ props.twitterDeletePost }
              favorites={ post.postData.retweetCount }
              retweets={ post.postData.favoriteCount }
              imgUrl={ imgUrl }
              id={ post.postData.id }
              type={ post.name }
              text={ post.postData.text }
            />
          );
        } else if (post.name === 'facebook' && post.postData.text) {
          return (
            <FacebookPost
              commentCount={ post.postData.commentCount }
              createDate={ formatDate }
              imgUrl={ post.postData.imageUrl }
              likeCount={ post.postData.likeCount }
              text={ post.postData.text }
              type={ post.name }
            />
          );
        } else if (post.name === 'facebook' && !post.postData.text) {
          return (
            <FacebookPost
              commentCount={ post.postData.commentCount }
              createDate={ formatDate }
              imgUrl={ post.postData.imageUrl }
              likeCount={ post.postData.likeCount }
              text={ '' }
              type={ post.name }
            />
          );
        }
      })
    }
    </div>
  );

  const allPosts = (
    <div className='allPosts'>
	  { dateFilter }
      { posts }
    </div>
  );

  const checkBoxes = _.map(props.platformNames, (name) => {
    return <div className='checkBox'>
		<CheckBox
		  name={ name }
		  onChange={ () => props.handlePostFilterChange(name) }
		  checked={ props.checkBoxState[name] }
		/>
	</div>
  });

  const filterCheckBoxes = (
    <div className='checkboxes'>
      { checkBoxes }
    </div>
  );

  const changeUI = (
    <DropMenu
      onChange={ props.changeUI }
      dropMenuDefault="Choose UI . . ."
      dropMenuOptions={
        [
          { id:2, value:'original', text:"Original UI" },
          { id:3, value:'sideNav', text:"Side Nav UI" }
        ]
      }
    />
  );

  const mainHeader = (
    <div className='header'>
      <Header headerItems={
          [
            filterCheckBoxes,
            searchBar,
            sortBy,
            changeUI,
						dataAnalysisNavButton,
            signOut
          ]
      } />
    </div>
  );

  const postButtonFooter = (
    <div className='postButtonFooter'>
			<Button className='square' onClick={ props.togglePopup } text="+Post" />
    </div>
  );

  const postPopupElements = (
      <div className='postPop'>
        <h4>Submit text and/or image post to platform</h4>
          <div>
            <textarea
							type='text'
              value={ props.postTextState() }
              onChange={ props.handlePostTextOnChange }
            />
          </div>
          <div>
             Upload Image <input type='file' name='postFile' onChange={ props.uploadFile }/>
          </div>
          <div className='previewImage'>
            { props.uploadImageDataUrlState &&
               <img
                  src={ props.uploadImageDataUrlState() }
                  width='100'
                  height='100'
                />
            }
          </div>
  		  <div className='submitButtons'>
  			{ twitterPostButton } { twitterPostImgButton }
  		  </div>
      </div>
  );

  const postPopup = (
    props.popupState &&
      <Popup
        closePopup={ props.togglePopup }
        elements={ postPopupElements }
        text='Create a New Post'
      />
  );

  const emptyPostsMessage = (
		<div className='postSpace'>
			<h3>There don't seem to be any posts here...</h3>
			<h4>Check off a social media box in the header to populate this area!</h4>
		</div>
  );

  const loginPopupButtons = (
    <div className='loginPop'>
        { twitterLoginButton }
        { facebookLoginComponent }
    </div>
  );

  const loginPopup = (
    props.loginPopupEnabled &&
    <Popup
      elements={ loginPopupButtons }
      text={ 'Log into your social media accounts' }
    />
  );

  const sideNavCheckBoxes = (
    <div className='sideCheckBoxes'>
      { checkBoxes }
    </div>
  );

  const sideNav = (
    <div className='sideNav'>
      <div className='sideElements'> <h2> Navigation </h2> </div>
      <div className='sideElements'> { changeUI } </div>
      <div className='sideElements'> { sortBy } </div>
      <div className='sideElements'> { searchBar } </div>
      <div className='sideElements'> { sideNavCheckBoxes } </div>
      <div className='sideElements'> { signOut } </div>
      <div className='sideElements'>
          <Button onClick={ props.togglePopup } text='POST' />
      </div>
    </div>
  );


  const rightOfNav = (
      <div className='rightOfNav'>
		{ dateFilter }
        { posts }
      </div>
  );

  if (props.stateOfUI() == 'original'){
  return (
    <div className='showcase'>
	  	{ emptyPostsMessage }
      { allPosts }
      { postButtonFooter }
	  	{ mainHeader }
	  	{ postPopup }
	  	{ loginPopup }
    </div>
  );
  }
  else {
    return (
      <div className='showcase'>
        { sideNav }
        { rightOfNav }
        { postPopup }
        { loginPopup }
      </div>
    );
  }
};

MainPage.propTypes = {
  changeSortState: PropTypes.func,
  changeSearchState: PropTypes.func,
  changeFilterState: PropTypes.func,
	changeUI: PropTypes.func,
  checkBoxState: PropTypes.object,
  facebookLogin: PropTypes.func.isRequired,
  getPostsToRender: PropTypes.func,
  handlePostFilterChange: PropTypes.func,
	handlePostTextOnChange: PropTypes.func.isRequired,
	loginPopupEnabled: PropTypes.bool.isRequired,
	navigateToDataPageOnClick: PropTypes.func.isRequired,
  platformNames: PropTypes.array.isRequired,
	popupState: PropTypes.bool,
	postTextState: PropTypes.func,
  redirectTolanding: PropTypes.func,
	stateOfUI: PropTypes.func,
	togglePopup: PropTypes.func,
	togglePostPlatforms: PropTypes.func,
  twitterTest: PropTypes.func,
  twitterLoginSuccess: PropTypes.func.isRequired,
  twitterLoginFailure: PropTypes.func.isRequired,
  uploadFile: PropTypes.func,
  uploadTweet: PropTypes.func
}
export default MainPage;
