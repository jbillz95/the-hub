import React from 'react';
import PropTypes from 'prop-types';
import './post.css';

// platform image urls
const imageUrls = {
	facebook: 'http://logovector.net/wp-content/uploads/2011/11/facebook-f-logo-195x195.png',
	instagram: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/100px-Instagram_logo_2016.svg.png',
	twitter: 'https://cdn1.iconfinder.com/data/icons/social-web-icons/154/twitter-bird-logo-logotype-128.png'
};

// creates image corresponding to platform chosen
const PlatformImage = (props) => {
	const url = imageUrls[props.platformName];

	if(!url) {
		return <div> not a valid platform </div>;
	}

	return (
		<div>
			<img src={ url } width='70' height='70' />
		</div>
	);
};

PlatformImage.propTypes = {
  platformName: PropTypes.string.isRequired
};

export default PlatformImage;
