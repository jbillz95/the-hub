import React from 'react';
import PropTypes from 'prop-types';
import { Post } from 'main-page';

const TwitterPost = (props) => {
  const metrics = {
    'Create Date': props.createDate,
    'Favorites': props.favorites,
    'Retweets': props.retweets
  };

  return (
    <Post
      className={ props.className }
      deleteTweet={ props.deleteTweet }
      id={ props.id }
      metrics={ metrics }
      type={ props.type }
      text={ props.text }
      imgUrl={ props.imgUrl }
    />
  );
};

TwitterPost.propTypes = {
  className: PropTypes.string,
  createDate: PropTypes.string.isRequired,
  favorites: PropTypes.number.isRequired,
  imgUrl: PropTypes.string,
  id: PropTypes.string.isRequired,
  retweets: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
}

export default TwitterPost;
