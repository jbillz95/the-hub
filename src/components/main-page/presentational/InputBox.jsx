import React from 'react';
import PropTypes from 'prop-types';

import './inputbox.css';

const InputBox = (props) => {
	return(
		<div>
			<input
				type="text"
				className={ props.className }
				placeholder={ props.placeholder }
				oninput={ props.onInput }
			/>
		</div>
	);
}

InputBox.propTypes = {
	className: PropTypes.string,
	placeholder: PropTypes.string,
	oninput: PropTypes.func.isRequired
}	

export default InputBox;