import React from 'react';
import PropTypes from 'prop-types';
import './dropmenu.css';

const DropMenuItem = (props) => {
	return(
		<option value={ props.value }>{ props.text }</option>
	);
}

DropMenuItem.propTypes = {
	value: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired
}

export default DropMenuItem;