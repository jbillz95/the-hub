import React from 'react';
import PropTypes from 'prop-types';

import './button.css';

const Button = (props) => {
	return (
		<div>
			<button
				className={ props.class }
				onClick={ props.onClick }
				type='button'
			>
			{ props.image }{ props.text }
			</button>
		</div>
	);
};

Button.propTypes = {
	class: PropTypes.string,
	image: PropTypes.any,
	onClick: PropTypes.func.isRequired,
	text: PropTypes.string.isRequired
}

export default Button;
