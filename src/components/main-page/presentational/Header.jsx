import React from 'react';
import './header.css';

const Header = (props) => {

	const headerContent = _.map(props.headerItems, (item) => 
		<div className='item'>{ item }</div>
	);

	return(
		<div className='head'>
			{ headerContent }
		</div>
	);
}

export default Header;
