import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MainPage, CheckBox } from 'main-page';
import { getCurrentUser, logoutCurrentUser, signIn, authState } from '../../../authentication.js';
import Post from '../presentational/Post.jsx';
import _ from 'lodash';

import {
  autoLoginUser,
  deleteTweet,
  getAllFacebookPostsFromFirebase,
  getAllTweetsFromFirebase,
  getFacebookPicturePosts,
  getFacebookPostMetrics,
  getFacebookProfile,
  getFacebookTextPosts,
  getLatestTweet,
  getUsersLatestTweets,
  onLoginFailure,
  onLoginSuccess,
  postTweet,
  postTweetImg,
  updateFacebookPostInFirebase,
  uploadAccessToken,
  uploadFacebookPostToFirebase,
  updateUsersTweetsInFirebase,
  uploadTweetToFirebase,
  getMostRecentTweetFromFirebase
} from 'apis';

//This MAY not be the best pattern
const PLATFORM_NAMES = ['facebook', 'twitter'];

class MainPageContainer extends React.Component {
  constructor(props) {
    super(props);

    const newState = _.merge(authState, {
      isOriginalUI: 'original',
      enabledPosts: {
        facebook: false,
        twitter: true
      },
      posts: {
        facebook: [],
        twitter: []
      },
      sortState: 'recent',
      searchState: '',
	  filterAfterState: null,
	  filterBeforeState: null,
      showPopup: false,
      uploadImageDataUrl: null,
      newPost: {
        platforms: {
          facebook: false,
          twitter: false,
        },
          text: 'example text'
      },
      profiles: {
        twitter: {},
        facebook: {}
      }
    });

    this.state = newState;
  }

  loginPopupEnabled = () => {
    const profiles = this.state.profiles;
    const enabled = _.isEmpty(profiles.twitter) || _.isEmpty(profiles.facebook);

    return enabled;
  }

  uploadTweetOnClick = () => {
    getUsersLatestTweets({ count: 10 }).then((tweets) => {
      return _.map(tweets, (tweet) => {
        //TODO: Consider exporting to a data model
        return {
          id: tweet.id_str,
          createdAt: new Date(tweet.created_at),
          text: tweet.text,
          retweetCount: tweet.retweet_count,
          favoriteCount: tweet.favorite_count,
          userId: tweet.user.id_str,
          userScreenName: tweet.user.screen_name,
          userName: tweet.user.name,
          media: _.get(tweet, 'entities.media') || false,
          popularity: (tweet.retweet_count + tweet.favorite_count)
        };
      });
    }).then((mappedTweets) => {
      const promises = _.map(mappedTweets, (tweet) => {
        return uploadTweetToFirebase(tweet);
      });
      return Promise.all(promises);
    }).then((tweets) => {
        console.log('All tweets successfully uploaded to firestore');

        const newState = _.cloneDeep(this.state);

        _.concat(newState.posts.twitter, tweets);

        this.setState(newState);
    }).catch((error) => {
      console.log(error);
    });
  }

  postToPlatformOnChange = (platformName) => {
    const newState = _.cloneDeep(this.state);

    newState.newPost.platforms[platformName] = !newState.newPost.platforms[platformName];

    this.setState(newState);
  }

  loadTweetsFromFirebase = () => {
    return getAllTweetsFromFirebase().then((tweets) => {
      const newState = _.cloneDeep(this.state);

      newState.posts.twitter = tweets;

      this.setState(newState);
    }).catch((error) => {
      console.log(error);
    });
  }

  loadFacebookPostsFromFirebase = () => {
    return getAllFacebookPostsFromFirebase().then((posts) => {
      return posts;
    }).catch((error) => {
      console.log(error);
    });
  }

  onTwitterLoginSuccess = (response) => {
    onLoginSuccess(response).then((user) => {
      const newState = _.cloneDeep(this.state);

      newState.profiles.twitter = user;
      this.setState(newState);
    }).then(() => {
      this.loadTweetsFromFirebase();
    });
  }

  checkBoxState = () => {
    const checkBoxesEnabled = {};
    _.forEach(PLATFORM_NAMES, (name) => {
      checkBoxesEnabled[name] = this.state.enabledPosts[name]
    });

    return checkBoxesEnabled;
  }

  changeMainPageUI = (event) => {
    this.setState({
          isOriginalUI: event.target.value
      });
  }

  togglePopup = () => {
    this.setState({
        showPopup: !this.state.showPopup,
        uploadImageDataUrl: null
    });
  }

  handlePostTextOnChange = (event) => {
      this.setState({
          newPost: {text: event.target.value}
      });
  }

  uploadFile = (event) => {
    event.preventDefault();
    let file = _.get(event, 'target.files[0]');

    if (file){
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          uploadImageDataUrl: reader.result
        });
      }
      reader.readAsDataURL(file);
    }
  }

  uploadImageDataUrlState = () => {
    return this.state.uploadImageDataUrl;
  }

  postTextState = () => {
    return this.state.newPost.text;
  }

  popupState = () => {
      const popupState = this.state.showPopup;
      return popupState;
  }

  stateOfUI = () => {
    return this.state.isOriginalUI;
  }

  changeSortState = (event) => {
    this.setState({
        sortState: event.target.value
    });
  }

  changeSearchState = (event) => {
    this.setState({
        searchState: event.target.value
    });
  }
  
  changeFilterState = (event) => {  
	if (event.target.name === 'after') {
		this.setState({
			filterAfterState: event.target.value
		});
	}
	if (event.target.name === 'before') {
		this.setState({
			filterBeforeState: event.target.value
		});
	}
  }

  navigateToDataPage = () => {
    this.props.history.push('/dataAnalysis');
  }

  dateCompare = (postDate, filter) => {
	  if (filter === 'after') {
		  const compDate = new Date(this.state.filterAfterState);
		  return compDate < postDate;
	  }
	  if (filter === 'before') {
		  const compDate = new Date(this.state.filterBeforeState);
		  return compDate > postDate;
	  }
  }
  
  //Helper function for aggregating posts from different platforms
  mergePosts = () => {
    const enabledPosts = this.state.enabledPosts;

    let facebookPosts = enabledPosts.facebook ? _.get(this.state, 'posts.facebook') : [];
    let twitterPosts = enabledPosts.twitter ? _.get(this.state, 'posts.twitter') : [];

    facebookPosts = facebookPosts || [];
    twitterPosts = twitterPosts || [];

    const posts = _.concat(facebookPosts, twitterPosts);
    if (_.isEmpty(posts)) {
      return [];
    }

    return posts;
  }

  //Helper function for searching/sorting posts
  organizePosts = (posts) => {
    const sortOptions = {
      'recent': (postData) => -postData.createdAt,
      'oldest' : (postData) => postData.createdAt,
      'popular' : (postData) => -postData.popularity,
      'least popular' : (postData) => postData.popularity,
    };

    let organizedPosts = _.sortBy(posts, sortOptions[this.state.sortState]);


    if(_.size(this.state.searchState) > 1) {
      organizedPosts = _.remove(organizedPosts, (postData) => {
        return _.toLower(postData.text).includes(_.toLower(this.state.searchState));
      });
    }
	
	if (this.state.filterAfterState) {
		organizedPosts = _.filter(organizedPosts, (postData) => {
			return this.dateCompare(postData.createdAt, 'after');
		});
	}
	
	if (this.state.filterBeforeState) {
		organizedPosts = _.filter(organizedPosts, (postData) => {
			return this.dateCompare(postData.createdAt, 'before');
		});
	}

    return organizedPosts;
  }

  postsToRender = () => {
    const allPosts = this.mergePosts();
    const organizedPosts = this.organizePosts(allPosts);

    return _.map(organizedPosts, (postData) => {
      const name = !_.isUndefined(postData.retweetCount) ? 'twitter' : 'facebook';
      return { name, postData };
    });
  }

    // redirects to landing page, isAuthed state is set to false
  redirectToLanding = () => {
    console.log('Signing Out');
    window.location.href ='';
  }

  /*
    When a checkbox is selected, toggle the enabledPosts on the state
  */
  handlePostFilterChange = (name) => {
    const newState = _.cloneDeep(this.state);
    newState.enabledPosts[name] = !newState.enabledPosts[name];

    this.setState(newState);
  };

  handleRefreshTweetsInFirebase = () => {
    const twitterUser = _.get(this.state, 'profiles.twitter');

    getAllTweetsFromFirebase().then((mostRecentTweet) => {
      //With the most recent tweet in hand, get all tweets after that one from twitter
      const options = {
        user_id: twitterUser.id_str,
        last_tweet_id: mostRecentTweet.id
      };

      updateUsersTweetsInFirebase(options).then(() => {
        console.log('Tweets successfully refreshed');
      }).catch((error) => {
        console.log(`ERROR: ${error}`);
      });
    }).catch((error) => {
      console.log(`ERROR: ${error}`);
    });
  }

  /*
    Takes in a string status to post to twitter, sends to the backend
    for posting, receives the tweet object, uploads that tweet to Firestore,
    finally pushes the new tweet onto the state.
    TODO: Wire up to posting popup (when developed)
    TODO: Handle non-text posts
  */
  handlePostTweet = (status) => {
    postTweet({ status }).then((result) => {
      //See if we got any errors (i.e. if it's a duplicate status)
      if (result.errors) {
        throw new Error(_.first(result.errors).message);
      }

      return result;
    }).then((tweet) => {
      return uploadTweetToFirebase({
        id: tweet.id_str,
        createdAt: new Date(tweet.created_at),
        text: tweet.text,
        retweetCount: tweet.retweet_count,
        favoriteCount: tweet.favorite_count,
        userId: tweet.user.id_str,
        userScreenName: tweet.user.screen_name,
        userName: tweet.user.name,
        media: _.get(tweet, 'entities.media') || false,
        popularity: (tweet.retweet_count + tweet.favorite_count)
      }).then((firebaseTweet) => {
        const newState = _.cloneDeep(this.state);
        newState.posts.twitter.push(firebaseTweet);

        this.setState(newState);
        this.togglePopup();
      }).catch((error) => {
        console.log(error);
      });
    }).catch((error) => {
      //If the user tried posting a duplicate status, alert them.
      alert(error);
      console.log(error);
    });
  }

  handlePostTweetImg = (status) => {
    const fullStatus = [];
    fullStatus.push(status);
    fullStatus.push(this.state.newPost.text);

    postTweetImg({ fullStatus }).then((result) => {
      
      if (result && result.errors) {
        throw new Error(_.first(result.errors).message);
      }
      this.togglePopup();
      return result;
    }).catch((error) => {
      alert(error);
      console.log(error);
    });
  }

  handleDeleteTweet = (id) => {
    deleteTweet({ id }).then((result) => {

      if (result && result.errors) {
        throw new Error(_.first(result.errors).message);
      }
      return result;
    }).then((theResult) => {
        _.forEach(this.state.posts['twitter'], (postData, index) => {
          if (postData.id === id){

            const newState = _.cloneDeep(this.state);
            newState.posts['twitter'].splice(index, 1);

            this.setState(newState);

            alert('tweet has been deleted');
          }
       });
    }).catch((error) => {
      alert(error);
      console.log(error);
    });
  }

  facebookLogin(response) {
    const fbProfile = {
      id: response.id,
      name: response.name,
      email: response.email
    };

    uploadAccessToken(response.accessToken).then(() => {
      const newState = _.cloneDeep(this.state);
      newState.profiles.facebook = fbProfile;

      this.setState(newState);
    }).catch((error) => {
      console.log(error);
    });
  }

  getFacebookProfile() {
    getFacebookProfile().then((fbProfile) => {
      if (fbProfile.error) {
        console.log('There was an error getting your Facebook profile');
        return;
      }

      const newState = _.cloneDeep(this.state);

      newState.profiles.facebook = fbProfile;

      this.setState(newState);
    }).catch((error) => {
      console.log(error);
    });
  }

  getFacebookPosts() {
    getFacebookTextPosts().then((textPosts) => {
      if (textPosts.error) {
        console.log('There was an error getting your Facebook posts');
        console.log(require('util').inspect(textPosts.error, { depth: null }));
        return;
      }

      //Now that we have all the posts in hand, we need to request their metrics
      const metricsPromises = _.map(textPosts, (post) => {
        return getFacebookPostMetrics(post.id).then((metrics) => {
          //Map metrics data to the post and return
          post.likeCount = metrics.likeCount;
          post.commentCount = metrics.commentCount;
          post.popularity = metrics.likeCount + (metrics.commentCount / 2);
          return post;
        });
      });

      //This will wait for all the promises to resolve before moving on
      return Promise.all(metricsPromises);
    }).then((postsWithMetrics) => {
      //Format the data how we want it in Firestore
      return _.map(postsWithMetrics, (post) => {
        return {
          id: post.id,
          createdAt: new Date(post.created_time),
          text: post.message,
          userId: post.from.id,
          userName: post.from.name,
          likeCount: post.likeCount,
          commentCount: post.commentCount,
          popularity: post.popularity
        };
      });
    }).then((formattedPostData) => {
      //Upload posts to Firestore
      const uploadPromises = _.map(formattedPostData, (post) => {
        return uploadFacebookPostToFirebase(post);
      });

      return Promise.all(uploadPromises);
    }).then((results) => {
      const newState = _.cloneDeep(this.state);

      newState.posts.facebook = results;
      this.setState(newState);
    })
    .catch((error) => {
      console.log(error);
    });
  };

  getFacebookPicPosts() {
    return getFacebookPicturePosts().then((picturePosts) => {
      if (picturePosts.error) {
        console.log('There was an error getting your Facebook picture posts');
        console.log(require('util').inspect(textPosts.error, { depth: null }));
        return;
      }

       const metricsPromises = _.map(picturePosts, (post) => {
        return getFacebookPostMetrics(post.id).then((metrics) => {
          post.likeCount = metrics.likeCount;
          post.commentCount = metrics.commentCount;
          post.popularity = metrics.likeCount + (metrics.commentCount / 2);
          return post;
        });
      });

      return Promise.all(metricsPromises);
    }).then((postsWithMetrics) => {
      return _.map(postsWithMetrics, (post) => {
        return {
          id: post.id,
          createdAt: new Date(post.created_time),
          text: post.message,
          userId: post.from.id,
          userName: post.from.name,
          imageUrl: post.full_picture,
          likeCount: post.likeCount,
          commentCount: post.commentCount,
          popularity: post.popularity
        };
      });
    }).catch((error) => {
      console.log(error);
    });
  };

  componentDidMount() {
    autoLoginUser().then((twitterProfile) => {
      if (twitterProfile) {
        const newState = this.state;
        newState.profiles.twitter = twitterProfile;

        this.setState(newState);
      }
    }).then(() => {
      return this.getFacebookProfile();
    }).then(() => {
      console.log('loading tweets from firebase');
      //IDEA: Refresh Tweets in Firebase...?
      return this.loadTweetsFromFirebase();
    }).then((tweets) => {
      console.log('loading Facebook posts from firebase');

      return this.loadFacebookPostsFromFirebase();
    }).then((allPosts) => {
      const clonedState = _.cloneDeep(this.state);

      clonedState.posts.facebook = allPosts;
      this.setState(clonedState);

      //TODO: We SHOULD have cached these in firebase...
      return this.getFacebookPicPosts();
    }).then((facebookPicPosts) => {
      const clonedState = _.cloneDeep(this.state);

      clonedState.posts.facebook = _.concat(clonedState.posts.facebook, facebookPicPosts);
      this.setState(clonedState);
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    return <MainPage
      changeSearchState={ this.changeSearchState }
      changeSortState={ this.changeSortState }
	  changeFilterState={ this.changeFilterState }
      changeUI={ this.changeMainPageUI }
      checkBoxState={ this.checkBoxState() }
      facebookLogin={ this.facebookLogin.bind(this) }
      getPostsToRender={ this.postsToRender }
      handlePostFilterChange={ this.handlePostFilterChange }
      handlePostTextOnChange={ this.handlePostTextOnChange }
      loginPopupEnabled={ this.loginPopupEnabled() }
      navigateToDataPageOnClick={ this.navigateToDataPage }
      platformNames={ PLATFORM_NAMES }
      popupState={ this.popupState() }
      postTextState={ this.postTextState }
      redirectToLanding={ this.redirectToLanding }
      stateOfUI={ this.stateOfUI }
      togglePopup={ this.togglePopup }
      togglePostPlatforms={ this.postToPlatformOnChange }
      twitterLoginSuccess={ this.onTwitterLoginSuccess }
      twitterLoginFailure={ onLoginFailure }
      twitterTest={ this.handlePostTweet }
      twitterTestImg={ this.handlePostTweetImg }
      twitterDeletePost= { this.handleDeleteTweet }
      uploadImageDataUrlState={ this.uploadImageDataUrlState }
      uploadTweet={ this.uploadTweetOnClick }
      uploadFile={ this.uploadFile }
    />;
  }
}

export default MainPageContainer;
