import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DataAnalysisPage } from 'data-analysis-page';
import { getCurrentUser, authState } from '../../../authentication.js';

class DataAnalysisPageContainer extends React.Component {
  constructor(props) {
    super(props);
    //TODO: Determine what parts of the state need to be carried over from MainPage
    //TODO: Pass in auth state...
  }

  render() {
    return <DataAnalysisPage/>;
  }
}

export default DataAnalysisPageContainer;
