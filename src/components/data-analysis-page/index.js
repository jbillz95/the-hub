import DataAnalysisPage from './presentational/DataAnalysisPage';
import DataAnalysisPageContainer from './containers/DataAnalysisPageContainer';

export {
  DataAnalysisPage,
  DataAnalysisPageContainer
};
