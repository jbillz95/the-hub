import React, { Component } from 'react';
import { LandingPage } from 'landing-page';
import { getCurrentUser, logoutCurrentUser, signIn, authState } from '../../../authentication.js';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';

class LandingPageContainer extends React.Component {
  constructor(props) {
    super(props);

    const newState = _.merge(authState, {showPopup: false});
    this.state = newState;
  }

  //TODO: Currently we have to press 'login' twice in order to set user data to state.
  loginOnClick = () => {
    signIn().then((res) => {
      getCurrentUser().then((user) => {
        authState.isAuthed = true;
        authState.user = user;
        this.setState(authState);
      }).then(() => {
        this.props.history.push('/main');
      });
    });
  };

  componentDidMount() {
    if (authState.isAuthed) {
      this.props.history.push('/main');
    } else {
      //Logging out the current user requires us to hit login twice to set user to state
      //logoutCurrentUser();
    }
  }

  togglePopup = () => {
    this.setState({
        showPopup: !this.state.showPopup
    });
  }

  popupState = () => {
      return this.state.showPopup; 
  }

  render() {
    return <LandingPage
      togglePopup={ this.togglePopup}
      popupState={ this.popupState() }
      signInUser={ this.loginOnClick }
    />;
  }
}

export default LandingPageContainer;
