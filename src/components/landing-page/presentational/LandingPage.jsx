import React, { Component } from 'react';
import './LandingPage.css';
import {  Button } from 'main-page';
import {
  Link,
  Redirect
} from 'react-router-dom';
import PropTypes from 'prop-types';

const LandingPage = (props) => {
  const loginButton = (
    <button onClick={ () => props.signInUser() }>Login</button>
  );

  const mainLink = (
    <Link to='/main'>Your Hub</Link>
  );

  const aboutContent = (
    <p> The Hub serves as a central hub for social media networks such
        as twitter and facebook. Users are able to post from here to their
        platforms.  They may also view all their posts in one place </p>
  );

  const popup = (
    props.popupState && <div className='about_popup'>
        <div className='about_inner'>
          <div className='about_elements'>
            <h1>About The Hub</h1>
            <div>  { aboutContent } </div>
            <Button class='back' text='Go Back' onClick={props.togglePopup} />
          </div>
        </div>
    </div>
  );

  const aboutButton = (
      <div><Button class='aboutButton' text='About' onClick={ props.togglePopup} /></div>
  );

  return (
    <div className='bodyDiv'>
      <div className='showcase'>
        <h1> The Hub </h1>
        <p> All your Posts. All in One Place</p>
          { loginButton }
          { mainLink }
          { aboutButton }
          { popup }
       </div>
    </div>
  )
};

LandingPage.propTypes = {
  signInUser: PropTypes.func.isRequired
}
export default LandingPage;
