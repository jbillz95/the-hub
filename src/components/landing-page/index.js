import LandingPageContainer from './container/LandingPageContainer';
import LandingPage from './presentational/LandingPage';

export {
  LandingPage,
  LandingPageContainer
};
