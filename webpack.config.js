/*
    ./webpack.config.js
*/
const path = require('path');

function resolvePath(part) {
  return path.resolve(__dirname, part);
}

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'the-hub.js',
    publicPath: '/'
  },
  mode: 'development',
  module: {
    rules: [
      { test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: "babel-loader"
      }, {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: "babel-loader"
      }
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  node: {
      console: true,
      fs: 'empty',
      net: 'empty',
      tls: 'empty'
  },
  resolve: {
    modules: ['node_modules'],
    alias: {
      'data-analysis-page': resolvePath('src/components/data-analysis-page/index.js'),
      'landing-page': resolvePath('src/components/landing-page/index.js'),
      'main-page': resolvePath('src/components/main-page/index.js'),
      'apis': resolvePath('src/apis/index.js')
    },
    extensions: ['.js', '.jsx', '.json']
  },
  plugins: [HtmlWebpackPluginConfig]
}
