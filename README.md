# Social Media Hub
## Setup Instructions
1. First, you will need install nvm which is used to manage versions of NodeJS. Installation instructions can be found [here](https://www.liquidweb.com/kb/how-to-install-nvm-node-version-manager-for-node-js-on-ubuntu-12-04-lts/).
2. With nvm installed, we may now install the appropriate version of NodeJS. Run the command `nvm install 8.9.1`. To verify that you are running the appropriate version, run `node -v`. If your output is "v8.9.1" then you're all set. Otherwise, use nvm to select the correct version (and probably set the default as well).
3. Now clone the bitbucket repo (if you don't know how to clone, Google it). This can be accomplished via the terminal or a GUI wrapper for git.
4. Navigate (in terminal) to wherever you cloned the repo to and run the command `sudo npm install npm --global` to get the latest version of npm (Node Package Manager).
  - This is a super critical part of your dev environment.
5. Make sure you're in the root project directory and then run the command `npm install`. This will install all of required packages for the project.
6. Finally, once that has finished it is time to start the app. Run `npm run start` and wait for it to boot up the dev server. To verify that it's working, go to your favorite web browser (Chrome) and go to `localhost:8080` and you should be greeted with a "Hello World" page.
